function Intro() {
    return (<div data-testid="introScreen">
        Welcome to a world ravaged by the undead, where humanity is on the brink of extinction. But amidst the chaos, there is still hope. The "Runners" are the brave souls who risk their lives to transport messages and goods between settlements, and act as forward scouts to keep the remaining survivors safe. Join them on their journey as they navigate this new world, and fight to bring humanity back from the brink. This is the story of the survivors, the runners, and the hope that keeps them going in a world overrun by zombies.       
    </div>)
}

export default Intro