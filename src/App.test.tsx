import { screen, render, fireEvent } from '@testing-library/react';
import App from './App';

test('render the title', async () => {
  render(<App />);
  await screen.findByText('Scouts on Zombie Road')
  expect(screen.getByText('Scouts on Zombie Road')).toBeInTheDocument();
});

test('can start game', async () => {
  render(<App />)
  const button = await screen.findByText('Start Game')
  expect(button).toBeInTheDocument()
})

test('click start game', async () => {
  render(<App />)
  expect(screen.getByTestId('introScreen')).toBeInTheDocument()
  const button = await screen.findByTestId('startGameButton')
  fireEvent.click(button)
  expect(button).toHaveTextContent('Restart')
  expect(screen.getByTestId('gameScreen')).toBeInTheDocument()
})
