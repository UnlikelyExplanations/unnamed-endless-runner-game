import { useState } from "react";
import "./App.css"
import Game from "./Game"
import Intro from "./Intro"

enum GameState {
  Unstarted,
  Started
}

function switchCaption(currentGameState: GameState) {
  return currentGameState === GameState.Started ? 'Restart' : 'Start Game';
}

function App() {
  const [currentGameState, setGameState] = useState(GameState.Unstarted)

  function changeState(): void {
    switch (currentGameState) {
      case GameState.Unstarted: setGameState(GameState.Started); break;
      default: setGameState(GameState.Unstarted); break;
    };
  }

  return (
    <div className="App">
      <h1>Scouts on Zombie Road</h1>
      <button onClick={() => changeState()} data-testid="startGameButton">{switchCaption(currentGameState)}</button>
      <div className="gameScreen">
        {currentGameState === GameState.Unstarted ? <Intro /> : <Game />}
      </div>
    </div>
  );
}

export default App;

